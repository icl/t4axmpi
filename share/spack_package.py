# vim: ts=4:sw=4:sts=4

from spack import *


class T4axmpi(AutotoolsPackage):
    """Tests 4 AX_MPI support."""

    homepage = "https://bitbucket.org/icl/t4axmpi"
    url      = "https://bitbucket.org/icl/t4axmpi/downloads/t4axmpi-0.0.1.tar.xz"

    version("0.0.2", "ae4f824b5d2e09cc457b810d3e49817f")
    version("0.0.1", "d5fb0ddbf74b9ae348735c74484579f1")

    depends_on("mpi")

    def dont_need_install(self, spec, prefix):
        mpicc = spec["mpi"].mpicc
        env["CC"] = mpicc
        configure("--prefix=" + prefix,
                  "--with-cc=%s" % mpicc,
                  "CC=%s" % mpicc)
        make()
        make("install")

    def configure_args(self):
        mpicc = self.spec["mpi"].mpicc
        return ["CC=" + mpicc]

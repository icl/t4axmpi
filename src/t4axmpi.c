/*
vim:sw=2
 */

#include <t4axmpi_conf.h>

#ifdef STDC_HEADERS
# include <string.h>
# include <stdio.h>
#endif

#ifdef HAVE_MPI_H
#include <mpi.h>
#endif

int process_args(int argc, char *argv[]) {
  int i;
  for (i = 1; i < argc && argv[i]; ++i)
    if (strncmp("--help", argv[i], 6) == 0)
      printf("%s [--help] [--version] [--mpi-version]\n", argv[0]);

    else if (strncmp("--version", argv[i], 9) == 0)
      printf("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);

    else if (strncmp("--mpi-version", argv[i], 13) == 0)
#if defined(HAVE_DECL___MPI_VERSION) && defined(HAVE_DECL_MPI_SUBVERSION___)
      printf("MPI %d.%d\n", MPI_VERSION, MPI_SUBVERSION);
#else
      printf("MPI UNKNOWN\n");
#endif
  return 0;
}


int main(int argc, char *argv[]) {
  int comm_rank;
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);

  if (0 == comm_rank)
    process_args(argc, argv);

  MPI_Finalize();
  return 0;
}
